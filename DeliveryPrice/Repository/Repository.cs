﻿using DeliveryPrice.Interfaces;
using DeliveryPrice.Models.Exceptions;
using System;
using System.Collections.Generic;

namespace DeliveryPrice.Repository
{
    public class Repository<T> : IRepository<T>
    {
        public readonly Dictionary<string, T> Storage = new Dictionary<string, T>();

        public T Get(DateTime dateTime)
        {
            var key = GetKey(dateTime);

            if (Storage.TryGetValue(key, out var item))
            {
                return item;
            }

            throw new NotExistException(key);
        }

        public void AddOrUpdate(DateTime dateTime, T item)
        {
            Storage[GetKey(dateTime)] = item;
        }

        private string GetKey(DateTime date) => date.ToString("yyyyMM");
    }
}