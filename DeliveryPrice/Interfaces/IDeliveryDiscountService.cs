﻿using DeliveryPrice.Models;

namespace DeliveryPrice.Interfaces
{
    public interface IDeliveryDiscountService
    {
        decimal Calculate(Transaction transaction);
    }
}