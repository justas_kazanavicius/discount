﻿using System.Collections.Generic;

namespace DeliveryPrice.Interfaces
{
    public interface IDeliveryDiscountRulesFactory
    {
        IEnumerable<IDeliveryDiscountRule> GetShipmentDiscountRules();
    }
}