﻿using System.Collections.Generic;

namespace DeliveryPrice.Interfaces
{
    public interface IDeliveryProvidersFactory
    {
        IEnumerable<IDeliveryProvider> GetDeliveryProvides();
    }
}