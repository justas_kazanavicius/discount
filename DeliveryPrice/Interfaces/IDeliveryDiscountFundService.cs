﻿using System;

namespace DeliveryPrice.Interfaces
{
    public interface IDeliveryDiscountFundService
    {
        decimal GetFunds(DateTime date);

        void UseFunds(DateTime date, decimal usedFunds);
    }
}