﻿using System;

namespace DeliveryPrice.Interfaces
{
    public interface IRepository<T>
    {
        T Get(DateTime dateTime);

        void AddOrUpdate(DateTime dateTime, T item);
    }
}