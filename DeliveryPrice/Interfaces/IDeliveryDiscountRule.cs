﻿using DeliveryPrice.Models;

namespace DeliveryPrice.Interfaces
{
    public interface IDeliveryDiscountRule
    {
        decimal Calculate(Transaction transaction, decimal fund);
    }
}