﻿using DeliveryPrice.Models;

namespace DeliveryPrice.Interfaces
{
    public interface IProvidersService
    {
        decimal GetLowestPrice(PackageSize packageSize);

        IDeliveryProvider GetProviderByCode(string code);
    }
}