﻿using DeliveryPrice.Models;

namespace DeliveryPrice.Interfaces
{
    public interface IDeliveryProvider
    {
        decimal GetPrice(PackageSize packageSize);

        string GetCode();
    }
}