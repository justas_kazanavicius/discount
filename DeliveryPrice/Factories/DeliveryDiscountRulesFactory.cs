﻿using DeliveryPrice.Interfaces;
using DeliveryPrice.Repository;
using DeliveryPrice.Rules;
using System.Collections.Generic;

namespace DeliveryPrice.Factories
{
    public class DeliveryDiscountRulesFactory : IDeliveryDiscountRulesFactory
    {
        private readonly IProvidersService _providersService;

        public DeliveryDiscountRulesFactory(IProvidersService providersService)
        {
            _providersService = providersService;
        }

        public IEnumerable<IDeliveryDiscountRule> GetShipmentDiscountRules()
        {
            yield return new DeliveryDiscountRuleLowestPackageS(_providersService);
            yield return new DeliveryDiscountRuleFreeThirdLp(new Repository<int>());
        }
    }
}