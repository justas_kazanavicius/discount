﻿using DeliveryPrice.Interfaces;
using DeliveryPrice.Providers;
using System.Collections.Generic;

namespace DeliveryPrice.Factories
{
    public class DeliveryProvidersFactory : IDeliveryProvidersFactory
    {
        public IEnumerable<IDeliveryProvider> GetDeliveryProvides()
        {
            yield return new DeliveryProviderLaPoste();
            yield return new DeliveryProviderMondialRelay();
        }
    }
}