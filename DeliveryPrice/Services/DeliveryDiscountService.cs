﻿using DeliveryPrice.Interfaces;
using DeliveryPrice.Models;

namespace DeliveryPrice.Services
{
    public class DeliveryDiscountService : IDeliveryDiscountService
    {
        private readonly IDeliveryDiscountFundService _fundService;
        private readonly IDeliveryDiscountRulesFactory _rulesFactory;

        public DeliveryDiscountService(IDeliveryDiscountFundService fundService, IDeliveryDiscountRulesFactory rulesFactory)
        {
            _fundService = fundService;
            _rulesFactory = rulesFactory;
        }

        public decimal Calculate(Transaction transaction)
        {
            var funds = _fundService.GetFunds(transaction.Date);

            return funds == 0 ? 0 : Calculate(transaction, funds);
        }

        private decimal Calculate(Transaction transaction, decimal funds)
        {
            var totalDiscount = 0m;

            foreach (var rule in _rulesFactory.GetShipmentDiscountRules())
            {
                var ruleDiscount = rule.Calculate(transaction, funds);

                funds -= ruleDiscount;
                totalDiscount += ruleDiscount;
            }

            _fundService.UseFunds(transaction.Date, totalDiscount);

            return totalDiscount;
        }
    }
}