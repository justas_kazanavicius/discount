﻿using DeliveryPrice.Interfaces;
using DeliveryPrice.Models;
using System.Linq;

namespace DeliveryPrice.Services
{
    public class ProvidersService : IProvidersService
    {
        private readonly IDeliveryProvidersFactory _deliveryProvidersFactory;

        public ProvidersService(IDeliveryProvidersFactory deliveryProvidersFactory)
        {
            _deliveryProvidersFactory = deliveryProvidersFactory;
        }

        public decimal GetLowestPrice(PackageSize packageSize)
        {
            return _deliveryProvidersFactory.GetDeliveryProvides().Min(x => x.GetPrice(packageSize));
        }

        public IDeliveryProvider GetProviderByCode(string code)
        {
            return _deliveryProvidersFactory.GetDeliveryProvides().Single(x => x.GetCode() == code);
        }
    }
}