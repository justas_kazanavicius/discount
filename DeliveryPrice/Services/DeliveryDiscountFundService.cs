﻿using DeliveryPrice.Interfaces;
using DeliveryPrice.Models.Exceptions;
using System;

namespace DeliveryPrice.Services
{
    public class DeliveryDiscountFundService : IDeliveryDiscountFundService
    {
        private readonly decimal _defaultMonthAmount = 10m;

        private readonly IRepository<decimal> _repository;

        public DeliveryDiscountFundService(IRepository<decimal> repository)
        {
            _repository = repository;
        }

        public decimal GetFunds(DateTime date)
        {
            try
            {
                return _repository.Get(date);
            }
            catch (NotExistException)
            {
                _repository.AddOrUpdate(date, _defaultMonthAmount);
                return _defaultMonthAmount;
            }
        }

        public void UseFunds(DateTime date, decimal usedFunds)
        {
            var funds = GetFunds(date);
            funds -= usedFunds;
            _repository.AddOrUpdate(date, funds);
        }
    }
}