﻿using DeliveryPrice.Interfaces;
using DeliveryPrice.Models;
using System;

namespace DeliveryPrice.Providers
{
    public class DeliveryProviderLaPoste : IDeliveryProviderLaPoste
    {
        public decimal GetPrice(PackageSize packageSize)
        {
            switch (packageSize)
            {
                case PackageSize.Small:
                    return 1.5m;

                case PackageSize.Medium:
                    return 4.9m;

                case PackageSize.Large:
                    return 6.9m;

                default:
                    throw new ArgumentOutOfRangeException(nameof(packageSize), packageSize, null);
            }
        }

        public string GetCode()
        {
            return "LP";
        }
    }
}