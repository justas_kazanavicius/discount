﻿using DeliveryPrice.Interfaces;
using DeliveryPrice.Models;
using System;

namespace DeliveryPrice.Providers
{
    public class DeliveryProviderMondialRelay : IDeliveryProviderMondialRelay
    {
        public decimal GetPrice(PackageSize packageSize)
        {
            switch (packageSize)
            {
                case PackageSize.Small:
                    return 2;

                case PackageSize.Medium:
                    return 3;

                case PackageSize.Large:
                    return 4;

                default:
                    throw new ArgumentOutOfRangeException(nameof(packageSize), packageSize, null);
            }
        }

        public string GetCode()
        {
            return "MR";
        }
    }
}