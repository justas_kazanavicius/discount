﻿using DeliveryPrice.Interfaces;
using DeliveryPrice.Models;

namespace DeliveryPrice.Rules
{
    public class DeliveryDiscountRuleLowestPackageS : DeliveryDiscountRule
    {
        private readonly IProvidersService _providersService;

        public DeliveryDiscountRuleLowestPackageS(IProvidersService providersService)
        {
            _providersService = providersService;
        }

        public override decimal Calculate(Transaction transaction)
        {
            if (transaction.PackageSize != PackageSize.Small) return 0;

            var actual = transaction.DeliveryProvider.GetPrice(transaction.PackageSize);
            var lowestPrice = _providersService.GetLowestPrice(PackageSize.Small);

            return actual - lowestPrice;
        }
    }
}