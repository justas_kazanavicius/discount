﻿using DeliveryPrice.Interfaces;
using DeliveryPrice.Models;
using DeliveryPrice.Models.Exceptions;
using System;

namespace DeliveryPrice.Rules
{
    public class DeliveryDiscountRuleFreeThirdLp : DeliveryDiscountRule
    {
        private readonly IRepository<int> _repository;

        public DeliveryDiscountRuleFreeThirdLp(IRepository<int> repository)
        {
            _repository = repository;
        }

        public override decimal Calculate(Transaction transaction)
        {
            return IsLaPosteDeliveryLargePacket(transaction) && IsThirdPacket(transaction)
                ? transaction.DeliveryProvider.GetPrice(PackageSize.Large)
                : 0;
        }

        private bool IsLaPosteDeliveryLargePacket(Transaction transaction)
        {
            return transaction.DeliveryProvider is IDeliveryProviderLaPoste
                   && transaction.PackageSize == PackageSize.Large;
        }

        private bool IsThirdPacket(Transaction transaction)
        {
            var number = GetNumber(transaction.Date);
            _repository.AddOrUpdate(transaction.Date, ++number);

            return number % 3 == 0;
        }

        private int GetNumber(DateTime date)
        {
            try
            {
                return _repository.Get(date);
            }
            catch (NotExistException)
            {
                _repository.AddOrUpdate(date, 0);
                return 0;
            }
        }
    }
}