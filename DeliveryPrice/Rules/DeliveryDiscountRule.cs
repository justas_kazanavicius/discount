﻿using DeliveryPrice.Interfaces;
using DeliveryPrice.Models;

namespace DeliveryPrice.Rules
{
    public abstract class DeliveryDiscountRule : IDeliveryDiscountRule
    {
        public abstract decimal Calculate(Transaction transaction);

        public decimal Calculate(Transaction transaction, decimal fund)
        {
            var fullDiscount = Calculate(transaction);

            return fullDiscount <= fund ? fullDiscount : fund;
        }
    }
}