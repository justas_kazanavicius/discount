﻿using DeliveryPrice.Interfaces;
using System;

namespace DeliveryPrice.Models
{
    public class Transaction
    {
        public DateTime Date { get; set; }
        public PackageSize PackageSize { get; set; }
        public IDeliveryProvider DeliveryProvider { get; set; }
    }
}