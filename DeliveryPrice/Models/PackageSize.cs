﻿namespace DeliveryPrice.Models
{
    public enum PackageSize
    {
        NotDefined, Small, Medium, Large
    }
}