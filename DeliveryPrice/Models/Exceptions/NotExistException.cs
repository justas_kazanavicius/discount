﻿using System;

namespace DeliveryPrice.Models.Exceptions
{
    public class NotExistException : Exception
    {
        public NotExistException(string key) : base(key)
        {
        }
    }
}