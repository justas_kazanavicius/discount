using DeliveryPrice.Interfaces;
using DeliveryPrice.Models.Exceptions;
using DeliveryPrice.Services;
using NSubstitute;
using NSubstitute.ExceptionExtensions;
using System;
using Xunit;

namespace DeliveryDiscount.UnitTests
{
    public class DeliveryDiscountFundServiceTests
    {
        private readonly IRepository<decimal> _repository = Substitute.For<IRepository<decimal>>();

        private readonly DeliveryDiscountFundService _fundService;

        public DeliveryDiscountFundServiceTests()
        {
            _fundService = new DeliveryDiscountFundService(_repository);
        }

        [Fact]
        public void CanGet()
        {
            var dateTime = new DateTime(2020, 01, 01);
            _repository.Get(dateTime).Returns(5);

            var actual = _fundService.GetFunds(dateTime);

            Assert.Equal(5, actual);
        }

        [Fact]
        public void CanGet_WhenNotExist()
        {
            var dateTime = new DateTime(2020, 01, 01);
            _repository.Get(dateTime).Throws(new NotExistException("key"));

            var actual = _fundService.GetFunds(new DateTime(2020, 01, 01));

            Assert.Equal(10, actual);
        }

        [Fact]
        public void CanUse()
        {
            var dateTime = new DateTime(2020, 01, 01);
            _repository.Get(dateTime).Returns(5);

            _fundService.UseFunds(dateTime, 4);

            _repository.AddOrUpdate(dateTime, 1);
        }
    }
}