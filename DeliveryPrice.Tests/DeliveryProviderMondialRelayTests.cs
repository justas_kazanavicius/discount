using DeliveryPrice.Models;
using DeliveryPrice.Providers;
using Xunit;

namespace DeliveryDiscount.UnitTests
{
    public class DeliveryProviderMondialRelayTests
    {
        private readonly DeliveryProviderMondialRelay _provider;

        public DeliveryProviderMondialRelayTests()
        {
            _provider = new DeliveryProviderMondialRelay();
        }

        [Theory]
        [InlineData(PackageSize.Small, 2)]
        [InlineData(PackageSize.Medium, 3)]
        [InlineData(PackageSize.Large, 4)]
        public void CanGetPrice(PackageSize packageSize, decimal price)
        {
            var actual = _provider.GetPrice(packageSize);

            Assert.Equal(price, actual);
        }

        [Fact]
        public void CanGetCode()
        {
            var actual = _provider.GetCode();

            Assert.Equal("MR", actual);
        }
    }
}