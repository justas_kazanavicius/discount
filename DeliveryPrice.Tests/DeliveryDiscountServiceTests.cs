using DeliveryPrice.Interfaces;
using DeliveryPrice.Models;
using DeliveryPrice.Services;
using NSubstitute;
using System;
using System.Collections.Generic;
using Xunit;

namespace DeliveryDiscount.UnitTests
{
    public class DeliveryDiscountServiceTests
    {
        private readonly IDeliveryDiscountFundService _fundService = Substitute.For<IDeliveryDiscountFundService>();

        private readonly IDeliveryDiscountRule _rule1 = Substitute.For<IDeliveryDiscountRule>();
        private readonly IDeliveryDiscountRule _rule2 = Substitute.For<IDeliveryDiscountRule>();
        private readonly IDeliveryDiscountRulesFactory _rulesFactory = Substitute.For<IDeliveryDiscountRulesFactory>();

        private readonly DeliveryDiscountService _service;

        public DeliveryDiscountServiceTests()
        {
            var rules = new List<IDeliveryDiscountRule> { _rule1, _rule2 };
            _rulesFactory.GetShipmentDiscountRules().Returns(rules);

            _service = new DeliveryDiscountService(_fundService, _rulesFactory);
        }

        [Fact]
        public void CanCalculate()
        {
            var transaction = new Transaction
            {
                Date = new DateTime(2020, 01, 01)
            };

            _fundService.GetFunds(transaction.Date).Returns(10);
            _rule1.Calculate(transaction, 10).Returns(4);
            _rule2.Calculate(transaction, 6).Returns(5);

            var actual = _service.Calculate(transaction);

            Assert.Equal(9, actual);
            _fundService.Received(1).UseFunds(transaction.Date, 9);
        }

        [Fact]
        public void CanCalculate_NoFunds()
        {
            var transaction = new Transaction
            {
                Date = new DateTime(2020, 01, 02)
            };

            _fundService.GetFunds(transaction.Date).Returns(0);

            var actual = _service.Calculate(transaction);

            Assert.Equal(0, actual);
            _fundService.Received(0).UseFunds(transaction.Date, Arg.Any<decimal>());
        }
    }
}