using DeliveryPrice.Models;
using DeliveryPrice.Rules;
using NSubstitute;
using Xunit;

namespace DeliveryDiscount.UnitTests
{
    public class DeliveryDiscountRuleTests
    {
        private readonly DeliveryDiscountRule _deliveryDiscountRule = Substitute.For<DeliveryDiscountRule>();

        [Fact]
        public void CanCalculate()
        {
            _deliveryDiscountRule.Calculate(Arg.Any<Transaction>()).Returns(8);

            var actual = _deliveryDiscountRule.Calculate(new Transaction(), 10);

            Assert.Equal(8, actual);
        }

        [Fact]
        public void CanCalculate_WhenNotEnoughFunds()
        {
            _deliveryDiscountRule.Calculate(Arg.Any<Transaction>()).Returns(8);

            var actual = _deliveryDiscountRule.Calculate(new Transaction(), 6);

            Assert.Equal(6, actual);
        }
    }
}