using DeliveryPrice.Models;
using DeliveryPrice.Providers;
using Xunit;

namespace DeliveryDiscount.UnitTests
{
    public class DeliveryProviderLaPosteTests
    {
        private readonly DeliveryProviderLaPoste _provider;

        public DeliveryProviderLaPosteTests()
        {
            _provider = new DeliveryProviderLaPoste();
        }

        [Theory]
        [InlineData(PackageSize.Small, 1.5)]
        [InlineData(PackageSize.Medium, 4.9)]
        [InlineData(PackageSize.Large, 6.9)]
        public void CanGetPrice(PackageSize packageSize, decimal price)
        {
            var actual = _provider.GetPrice(packageSize);

            Assert.Equal(price, actual);
        }

        [Fact]
        public void CanGetCode()
        {
            var actual = _provider.GetCode();

            Assert.Equal("LP", actual);
        }
    }
}