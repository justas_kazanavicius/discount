using DeliveryPrice.Interfaces;
using DeliveryPrice.Models;
using DeliveryPrice.Models.Exceptions;
using DeliveryPrice.Rules;
using NSubstitute;
using NSubstitute.ExceptionExtensions;
using System;
using Xunit;

namespace DeliveryDiscount.UnitTests
{
    public class DeliveryDiscountRuleFreeThirdLpTests
    {
        private readonly IRepository<int> _repository = Substitute.For<IRepository<int>>();
        private readonly IDeliveryProviderLaPoste _deliveryProvider = Substitute.For<IDeliveryProviderLaPoste>();

        private readonly DeliveryDiscountRuleFreeThirdLp _rule;

        private readonly Transaction _transaction;
        private readonly DateTime _dateTime;

        public DeliveryDiscountRuleFreeThirdLpTests()
        {
            _dateTime = new DateTime(2020, 01, 02);

            _transaction = new Transaction
            {
                Date = _dateTime,
                PackageSize = PackageSize.Large,
                DeliveryProvider = _deliveryProvider
            };

            _repository.Get(_dateTime).Returns(2);

            _deliveryProvider.GetPrice(PackageSize.Large).Returns(5);

            _rule = new DeliveryDiscountRuleFreeThirdLp(_repository);
        }

        [Fact]
        public void CanCalculate()
        {
            var actual = _rule.Calculate(_transaction);

            Assert.Equal(5, actual);
            _repository.Received(1).AddOrUpdate(_dateTime, 3);
        }

        [Fact]
        public void CanCalculate_WhenNotLarge()
        {
            _transaction.PackageSize = PackageSize.Medium;
            var actual = _rule.Calculate(_transaction);

            Assert.Equal(0, actual);
        }

        [Fact]
        public void CanCalculate_WhenNotLaPoste()
        {
            _transaction.PackageSize = PackageSize.Medium;
            var actual = _rule.Calculate(_transaction);

            Assert.Equal(0, actual);
        }

        [Fact]
        public void CanCalculate_WhenNotThird()
        {
            _repository.Get(_dateTime).Returns(1);
            var actual = _rule.Calculate(_transaction);

            Assert.Equal(0, actual);
        }

        [Fact]
        public void CanCalculate_WhenNoRecords()
        {
            _repository.Get(_dateTime).Throws(new NotExistException("key"));
            var actual = _rule.Calculate(_transaction);

            Assert.Equal(0, actual);
        }
    }
}