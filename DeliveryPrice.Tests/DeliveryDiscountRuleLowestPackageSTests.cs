using DeliveryPrice.Interfaces;
using DeliveryPrice.Models;
using DeliveryPrice.Rules;
using NSubstitute;
using Xunit;

namespace DeliveryDiscount.UnitTests
{
    public class DeliveryDiscountRuleLowestPackageSTests
    {
        private readonly IProvidersService _providersService = Substitute.For<IProvidersService>();
        private readonly IDeliveryProvider _deliveryProvider = Substitute.For<IDeliveryProvider>();

        private readonly DeliveryDiscountRuleLowestPackageS _ruleLowestPackageS;

        public DeliveryDiscountRuleLowestPackageSTests()
        {
            _providersService.GetLowestPrice(PackageSize.Small).Returns(1);

            _ruleLowestPackageS = new DeliveryDiscountRuleLowestPackageS(_providersService);
        }

        [Theory]
        [InlineData(2, 1)]
        [InlineData(1, 0)]
        public void CanCalculate(decimal providerPrice, decimal discount)
        {
            var transaction = new Transaction
            {
                PackageSize = PackageSize.Small,
                DeliveryProvider = _deliveryProvider
            };

            _deliveryProvider.GetPrice(PackageSize.Small).Returns(providerPrice);

            var actual = _ruleLowestPackageS.Calculate(transaction);

            Assert.Equal(discount, actual);
        }
    }
}