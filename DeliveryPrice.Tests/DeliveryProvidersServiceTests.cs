using DeliveryPrice.Interfaces;
using DeliveryPrice.Models;
using DeliveryPrice.Services;
using NSubstitute;
using System.Collections.Generic;
using Xunit;

namespace DeliveryDiscount.UnitTests
{
    public class DeliveryProvidersServiceTests
    {
        private readonly IDeliveryProvider _deliveryProvider1 = Substitute.For<IDeliveryProvider>();
        private readonly IDeliveryProvider _deliveryProvider2 = Substitute.For<IDeliveryProvider>();
        private readonly IDeliveryProvidersFactory _deliveryProvidersFactory = Substitute.For<IDeliveryProvidersFactory>();

        private readonly ProvidersService _service;

        public DeliveryProvidersServiceTests()
        {
            _deliveryProvider1.GetPrice(PackageSize.Small).Returns(2);
            _deliveryProvider2.GetPrice(PackageSize.Small).Returns(1);

            _deliveryProvider1.GetCode().Returns("AA");
            _deliveryProvider2.GetCode().Returns("BB");

            _deliveryProvidersFactory.GetDeliveryProvides().Returns(new List<IDeliveryProvider>
            {
                _deliveryProvider1,
                _deliveryProvider2
            });

            _service = new ProvidersService(_deliveryProvidersFactory);
        }

        [Fact]
        public void GetLowestPrice()
        {
            var actual = _service.GetLowestPrice(PackageSize.Small);

            Assert.Equal(1, actual);
        }

        [Fact]
        public void GetLowest()
        {
            var actual = _service.GetProviderByCode("BB");
            Assert.Same(_deliveryProvider2, actual);
        }
    }
}