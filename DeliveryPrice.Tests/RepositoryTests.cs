using DeliveryPrice.Models.Exceptions;
using DeliveryPrice.Repository;
using System;
using Xunit;

namespace DeliveryDiscount.UnitTests
{
    public class RepositoryTests
    {
        private readonly Repository<int> _repository;

        public RepositoryTests()
        {
            _repository = new Repository<int>();
        }

        [Fact]
        public void CanGet()
        {
            _repository.Storage.Add("202011", 15);

            var actual = _repository.Get(new DateTime(2020, 11, 15));

            Assert.Equal(15, actual);
        }

        [Fact]
        public void CanGet_WhenNotExist()
        {
            Assert.Throws<NotExistException>(() => _repository.Get(new DateTime(2020, 11, 15)));
        }

        [Fact]
        public void AddOrUpdate_WhenUpdate()
        {
            _repository.Storage.Add("202011", 15);

            _repository.AddOrUpdate(new DateTime(2020, 11, 15), 10);

            Assert.Equal(10, _repository.Storage["202011"]);
        }

        [Fact]
        public void AddOrUpdate_WhenAdd()
        {
            _repository.AddOrUpdate(new DateTime(2020, 11, 15), 9);

            Assert.Equal(9, _repository.Storage["202011"]);
        }
    }
}