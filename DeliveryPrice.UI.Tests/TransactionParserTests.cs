using DeliveryPrice.Interfaces;
using DeliveryPrice.Models;
using DeliveryPrice.UI.Models;
using DeliveryPrice.UI.Parsers;
using NSubstitute;
using NSubstitute.ExceptionExtensions;
using System;
using Xunit;
using Xunit.Asserts.Compare;

namespace DeliveryPrice.UI.Tests
{
    public class TransactionParserTests
    {
        private readonly IDeliveryProvider _deliveryProvider = Substitute.For<IDeliveryProvider>();
        private readonly IProvidersService _providersService = Substitute.For<IProvidersService>();

        private readonly TransactionParser _parser;

        public TransactionParserTests()
        {
            _providersService.GetProviderByCode("MR").Returns(_deliveryProvider);
            _providersService.GetProviderByCode("AA").Throws(new Exception());

            _parser = new TransactionParser(_providersService);
        }

        [Theory]
        [InlineData("2020-01-01 S MR", PackageSize.Small)]
        [InlineData("2020-01-01 M MR", PackageSize.Medium)]
        [InlineData("2020-01-01 L MR", PackageSize.Large)]
        public void CanParse(string input, PackageSize packageSize)
        {
            var expected = new TransactionInput
            {
                Transaction = new Transaction
                {
                    Date = new DateTime(2020, 01, 01),
                    PackageSize = packageSize,
                    DeliveryProvider = _deliveryProvider
                },
                Ignored = null
            };

            var actual = _parser.Parse(input);

            DeepAssert.Equal(expected, actual);
        }

        [Theory]
        [InlineData("a")]
        [InlineData("a b c d")]
        [InlineData("2020 01 01 S MR")]
        [InlineData("2020-01-01 D MR")]
        [InlineData("2020-01-01 S AA")]
        public void CanParse_WhenIgnore(string input)
        {
            var actual = _parser.Parse(input);

            DeepAssert.Equal(input, actual.Ignored);
        }
    }
}