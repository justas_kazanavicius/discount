using DeliveryPrice.UI.Interfaces;
using DeliveryPrice.UI.Models;
using DeliveryPrice.UI.Parsers;
using NSubstitute;
using System.Collections.Generic;
using System.Linq;
using Xunit;
using Xunit.Asserts.Compare;

namespace DeliveryPrice.UI.Tests
{
    public class FileParserTests
    {
        private readonly IFileReader _fileReader = Substitute.For<IFileReader>();
        private readonly ITransactionParser _transactionParser = Substitute.For<ITransactionParser>();

        private readonly FileParser _fileParser;

        public FileParserTests()
        {
            _fileReader.ReadLines("file").Returns(new List<string> { "line1", "line2" });

            _transactionParser.Parse("line1").Returns(new TransactionInput { Ignored = "Ignored" });
            _transactionParser.Parse("line2").Returns(new TransactionInput { Ignored = null });

            _fileParser = new FileParser(_fileReader, _transactionParser);
        }

        [Fact]
        public void CanParse()
        {
            var expected = new List<TransactionInput>
            {
                new TransactionInput { Ignored = "Ignored"  },
                new TransactionInput { Ignored = null }
            };

            var actual = _fileParser.ParseTransactions("file").ToList();

            DeepAssert.Equal(expected, actual);
        }
    }
}