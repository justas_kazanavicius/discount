using DeliveryPrice.Interfaces;
using DeliveryPrice.Models;
using DeliveryPrice.UI.Services;
using NSubstitute;
using System;
using Xunit;
using Xunit.Asserts.Compare;

namespace DeliveryPrice.UI.Tests
{
    public class DeliveryPriceServiceTests
    {
        private readonly IDeliveryProvider _deliveryProvider = Substitute.For<IDeliveryProvider>();
        private readonly IDeliveryDiscountService _discountService = Substitute.For<IDeliveryDiscountService>();

        private readonly DeliveryPriceService _deliveryPriceService;
        private readonly Transaction _transaction;

        public DeliveryPriceServiceTests()
        {
            _transaction = new Transaction
            {
                Date = new DateTime(2020, 01, 01),
                PackageSize = PackageSize.Small,
                DeliveryProvider = _deliveryProvider
            };

            _deliveryProvider.GetPrice(_transaction.PackageSize).Returns(11);
            _discountService.Calculate(_transaction).Returns(5);

            _deliveryPriceService = new DeliveryPriceService(_discountService);
        }

        [Fact]
        public void CanCalculate()
        {
            var expected = new Models.DeliveryPrice { Price = 6, Discount = 5 };

            var actual = _deliveryPriceService.Calculate(_transaction);

            DeepAssert.Equal(expected, actual);
        }
    }
}