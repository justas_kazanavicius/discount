﻿using DeliveryPrice.UI.Models;
using System.Collections.Generic;

namespace DeliveryPrice.UI.Interfaces
{
    public interface IFileParser
    {
        IEnumerable<TransactionInput> ParseTransactions(string file);
    }
}