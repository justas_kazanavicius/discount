﻿using DeliveryPrice.UI.Models;

namespace DeliveryPrice.UI.Interfaces
{
    public interface ITransactionParser
    {
        TransactionInput Parse(string line);
    }
}