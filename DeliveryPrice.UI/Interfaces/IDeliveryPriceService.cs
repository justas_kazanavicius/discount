﻿using DeliveryPrice.Models;

namespace DeliveryPrice.UI.Interfaces
{
    public interface IDeliveryPriceService
    {
        Models.DeliveryPrice Calculate(Transaction transaction);
    }
}