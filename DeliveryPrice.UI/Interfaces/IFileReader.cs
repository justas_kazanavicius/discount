﻿using System.Collections.Generic;

namespace DeliveryPrice.UI.Interfaces
{
    public interface IFileReader
    {
        IEnumerable<string> ReadLines(string file);
    }
}