﻿using DeliveryPrice.UI.Interfaces;
using System.Collections.Generic;
using System.IO;

namespace DeliveryPrice.UI.Readers
{
    public class FileReader : IFileReader
    {
        public IEnumerable<string> ReadLines(string file)
        {
            return File.ReadLines(file);
        }
    }
}