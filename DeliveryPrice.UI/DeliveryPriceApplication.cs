﻿using DeliveryPrice.Models;
using DeliveryPrice.UI.Interfaces;
using DeliveryPrice.UI.Models;
using System;
using System.Globalization;
using System.Linq;

namespace DeliveryPrice.UI
{
    public class DeliveryPriceApplication
    {
        private readonly IFileParser _fileParser;
        private readonly IDeliveryPriceService _deliveryPriceService;

        public DeliveryPriceApplication(IFileParser fileParser, IDeliveryPriceService deliveryPriceService)
        {
            _fileParser = fileParser;
            _deliveryPriceService = deliveryPriceService;
        }

        public void ProcessFile(string file)
        {
            try
            {
                ProcessTransactions(file);
                Console.WriteLine($"File: {file} successfully processed");
            }
            catch
            {
                //log exception details if needed
                Console.WriteLine($"Failed to process file: {file}");
            }
        }

        private void ProcessTransactions(string file)
        {
            foreach (var input in _fileParser.ParseTransactions(file))
            {
                ProcessTransaction(input);
            }
        }

        private void ProcessTransaction(TransactionInput input)
        {
            if (input.Ignored != null)
            {
                PrintIgnored(input.Ignored);
            }
            else
            {
                var calculated = _deliveryPriceService.Calculate(input.Transaction);
                PrintDiscount(input.Transaction, calculated);
            }
        }

        private void PrintIgnored(string text)
        {
            Console.WriteLine($"{text} Ignored");
        }

        private void PrintDiscount(Transaction transaction, Models.DeliveryPrice deliveryPrice)
        {
            var discount = deliveryPrice.Discount != 0
                ? deliveryPrice.Discount.ToString("F2", CultureInfo.InvariantCulture)
                : "-";

            Console.WriteLine($"{transaction.Date:yyyy-MM-dd} " +
                              $"{transaction.PackageSize.ToString().First()} " +
                              $"{transaction.DeliveryProvider.GetCode()} " +
                              $"{deliveryPrice.Price.ToString("F2", CultureInfo.InvariantCulture)} " +
                              $"{discount}");
        }
    }
}