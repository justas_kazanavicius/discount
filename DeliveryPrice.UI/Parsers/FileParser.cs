﻿using DeliveryPrice.UI.Interfaces;
using DeliveryPrice.UI.Models;
using System.Collections.Generic;

namespace DeliveryPrice.UI.Parsers
{
    public class FileParser : IFileParser
    {
        private readonly IFileReader _fileReader;
        private readonly ITransactionParser _transactionParser;

        public FileParser(IFileReader fileReader, ITransactionParser transactionParser)
        {
            _fileReader = fileReader;
            _transactionParser = transactionParser;
        }

        public IEnumerable<TransactionInput> ParseTransactions(string file)
        {
            var allLines = _fileReader.ReadLines(file);

            foreach (var line in allLines)
            {
                yield return _transactionParser.Parse(line);
            }
        }
    }
}