﻿using DeliveryPrice.Interfaces;
using DeliveryPrice.Models;
using DeliveryPrice.UI.Interfaces;
using DeliveryPrice.UI.Models;
using System;
using System.Globalization;

namespace DeliveryPrice.UI.Parsers
{
    public class TransactionParser : ITransactionParser
    {
        private readonly IProvidersService _providersService;

        public TransactionParser(IProvidersService providersService)
        {
            _providersService = providersService;
        }

        public TransactionInput Parse(string line)
        {
            var inputs = line.Split(" ");

            if (inputs.Length != 3
            || !TryToParseDate(inputs[0], out var date)
            || !TryToParsePacketSize(inputs[1], out var packageSize)
            || !TryToParseDeliverProvider(inputs[2], out var deliveryProvider)) return Ignored(line);

            return new TransactionInput
            {
                Transaction = new Transaction
                {
                    Date = date,
                    PackageSize = packageSize,
                    DeliveryProvider = deliveryProvider
                }
            };
        }

        private TransactionInput Ignored(string line)
        {
            return new TransactionInput { Ignored = line };
        }

        private bool TryToParseDate(string input, out DateTime date)
        {
            return DateTime.TryParseExact(input, "yyyy-MM-dd", CultureInfo.InvariantCulture, DateTimeStyles.None, out date);
        }

        private bool TryToParsePacketSize(string input, out PackageSize packageSize)
        {
            switch (input)
            {
                case "S":
                    packageSize = PackageSize.Small;
                    return true;

                case "M":
                    packageSize = PackageSize.Medium;
                    return true;

                case "L":
                    packageSize = PackageSize.Large;
                    return true;

                default:
                    packageSize = PackageSize.NotDefined;
                    return false;
            }
        }

        private bool TryToParseDeliverProvider(string input, out IDeliveryProvider deliveryProvider)
        {
            try
            {
                deliveryProvider = _providersService.GetProviderByCode(input);
                return true;
            }
            catch
            {
                deliveryProvider = null;
                return false;
            }
        }
    }
}