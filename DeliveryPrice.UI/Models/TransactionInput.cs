﻿using DeliveryPrice.Models;

namespace DeliveryPrice.UI.Models
{
    public class TransactionInput
    {
        public string Ignored { get; set; }

        public Transaction Transaction { get; set; }
    }
}