﻿namespace DeliveryPrice.UI.Models
{
    public class DeliveryPrice
    {
        public decimal Price { get; set; }
        public decimal Discount { get; set; }
    }
}