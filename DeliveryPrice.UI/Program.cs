﻿using System;
using DeliveryPrice.UI.Factories;

namespace DeliveryPrice.UI
{
    internal class Program
    {
        private static readonly DeliveryPriceApplication DeliveryPriceApplication = DeliveryPriceApplicationFactory.Create();

        private static void Main(string[] args)
        {
#if DEBUG
            args = new[] { "input.txt" };
#endif

            if (args.Length == 1)
            {
                DeliveryPriceApplication.ProcessFile(args[0]);
                return;
            }

            PrintUsage();
        }

        private static void PrintUsage()
        {
            Console.WriteLine("Please specify input file!");
            Console.WriteLine($"Example: {AppDomain.CurrentDomain.FriendlyName} input.txt");
        }
    }
}