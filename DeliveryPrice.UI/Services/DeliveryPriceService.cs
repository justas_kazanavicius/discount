﻿using DeliveryPrice.Interfaces;
using DeliveryPrice.Models;
using DeliveryPrice.UI.Interfaces;

namespace DeliveryPrice.UI.Services
{
    public class DeliveryPriceService : IDeliveryPriceService
    {
        private readonly IDeliveryDiscountService _discountService;

        public DeliveryPriceService(IDeliveryDiscountService discountService)
        {
            _discountService = discountService;
        }

        public Models.DeliveryPrice Calculate(Transaction transaction)
        {
            var primaryPrice = transaction.DeliveryProvider.GetPrice(transaction.PackageSize);
            var discount = _discountService.Calculate(transaction);
            var finallyPrice = primaryPrice - discount;

            return new Models.DeliveryPrice
            {
                Price = finallyPrice,
                Discount = discount
            };
        }
    }
}