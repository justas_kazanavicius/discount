﻿using DeliveryPrice.Factories;
using DeliveryPrice.Repository;
using DeliveryPrice.Services;
using DeliveryPrice.UI.Parsers;
using DeliveryPrice.UI.Readers;
using DeliveryPrice.UI.Services;

namespace DeliveryPrice.UI.Factories
{
    public static class DeliveryPriceApplicationFactory
    {
        public static DeliveryPriceApplication Create()
        {
            var fileReader = new FileReader();
            var providersFactory = new DeliveryProvidersFactory();
            var providersService = new ProvidersService(providersFactory);
            var transactionParser = new TransactionParser(providersService);
            var fileParser = new FileParser(fileReader, transactionParser);
            var repository = new Repository<decimal>();
            var deliveryDiscountFundService = new DeliveryDiscountFundService(repository);
            var deliveryDiscountRulesFactory = new DeliveryDiscountRulesFactory(providersService);
            var deliveryDiscountService = new DeliveryDiscountService(deliveryDiscountFundService, deliveryDiscountRulesFactory);
            var deliveryPriceService = new DeliveryPriceService(deliveryDiscountService);

            return new DeliveryPriceApplication(fileParser, deliveryPriceService);
        }
    }
}